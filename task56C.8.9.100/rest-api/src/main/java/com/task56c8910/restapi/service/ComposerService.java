package com.task56c8910.restapi.service;

import java.util.ArrayList;
import java.util.List;

import com.task56c8910.restapi.model.Artist;
import com.task56c8910.restapi.model.Band;
import com.task56c8910.restapi.model.BandMember;
import com.task56c8910.restapi.model.Composer;

public class ComposerService {
    private static List<Composer> listComposer = new ArrayList<Composer>();
    private static List<Band> listBand = new ArrayList<Band>();
    private static List<Artist> listArtist = new ArrayList<Artist>();

    static {
        Artist artist1 = new Artist("nam", "nguyen", "stage 1", AlbumService.getListAlbum());
        Artist artist2 = new Artist("van", "nguyen", "stage 2", AlbumService.getListAlbum());
        Artist artist3 = new Artist("mai", "nguyen", "stage 3", AlbumService.getListAlbum());
        BandMember member1 = new BandMember("ha", "nguyen", "stage 1", "piano");
        BandMember member2 = new BandMember("lan", "nguyen", "stage 2", "piano");
        BandMember member3 = new BandMember("hung", "nguyen", "stage 3", "piano");
        BandMember member4 = new BandMember("dung", "nguyen", "stage 1", "drum");
        BandMember member5 = new BandMember("truc", "nguyen", "stage 2", "drum");
        BandMember member6 = new BandMember("chi", "nguyen", "stage 3", "drum");

        listComposer.add(artist1);
        listComposer.add(artist2);
        listComposer.add(artist3);
        listComposer.add(member1);
        listComposer.add(member2);
        listComposer.add(member3);
        listComposer.add(member4);
        listComposer.add(member5);
        listComposer.add(member6);
        List<BandMember> bandmember1 = new ArrayList<BandMember>();
        bandmember1.add(member1);
        bandmember1.add(member2);
        List<BandMember> bandmember2 = new ArrayList<BandMember>();
        bandmember2.add(member3);
        bandmember2.add(member4);
        Band band1 = new Band("band1", bandmember1, AlbumService.getListAlbum());
        Band band2 = new Band("band2", bandmember2, AlbumService.getListAlbum());
        listBand.add(band1);
        listBand.add(band2);

        listArtist.add(artist1);
        listArtist.add(artist2);
        listArtist.add(artist3);

    }

    public static List<Composer> getListComposer() {
        return listComposer;
    }

    public static List<Band> getListBand() {
        return listBand;
    }

    public static void setListComposer(List<Composer> listComposer) {
        ComposerService.listComposer = listComposer;
    }

    public static void setListBand(List<Band> listBand) {
        ComposerService.listBand = listBand;
    }

    public static List<Artist> getListArtist() {
        return listArtist;
    }

    public static void setListArtist(List<Artist> listArtist) {
        ComposerService.listArtist = listArtist;
    }

}
