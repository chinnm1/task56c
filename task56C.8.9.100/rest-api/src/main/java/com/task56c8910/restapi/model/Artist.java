package com.task56c8910.restapi.model;

import java.util.List;

public class Artist extends Composer {
    private List<Album> albums;

    public Artist(String firstName, String lastName, String stagename, List<Album> albums) {
        super(firstName, lastName, stagename);
        this.albums = albums;
    }

    public List<Album> getAlbums() {
        return albums;
    }

}
