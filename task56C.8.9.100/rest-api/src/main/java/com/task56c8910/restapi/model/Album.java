package com.task56c8910.restapi.model;

import java.util.List;

public class Album {
    private String name;
    private List<String> songs;

    public Album(String name, List<String> songs) {
        this.name = name;
        this.songs = songs;
    }

    public Album() {
    }

    public String getAlbumName() {
        return name;
    }

    public List<String> getSongs() {
        return songs;
    }

}
