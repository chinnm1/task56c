package com.task56c8910.restapi.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.task56c8910.restapi.model.Album;
import com.task56c8910.restapi.model.Artist;
import com.task56c8910.restapi.service.AlbumService;
import com.task56c8910.restapi.service.ComposerService;

@RestController
public class AlbumController {
    @CrossOrigin
    @GetMapping("/all-albums")
    public List<Album> getAlbum() throws Exception {
        List<Album> allAlbums = AlbumService.getListAlbum();
        return allAlbums;
    }

    @GetMapping("/all-artist")
    public List<Artist> getArtist() throws Exception {
        List<Artist> allArtists = ComposerService.getListArtist();
        return allArtists;
    }

}
