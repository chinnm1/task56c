package com.task56c8910.restapi.model;

public class BandMember extends Composer {
    private String instrument;

    public BandMember(String firstName, String lastName, String stagename, String instrument) {
        super(firstName, lastName, stagename);
        this.instrument = instrument;
    }

    public String getInstrument() {
        return instrument;
    }

}
