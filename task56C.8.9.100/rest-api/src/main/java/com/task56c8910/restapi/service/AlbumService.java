package com.task56c8910.restapi.service;

import java.util.ArrayList;
import java.util.List;

import com.task56c8910.restapi.model.Album;

public class AlbumService {
    private static List<Album> listAlbum = new ArrayList<Album>();
    static {
        List<String> song1 = new ArrayList<>();
        song1.add("song1-1");
        song1.add("song1-2");
        List<String> song2 = new ArrayList<>();
        song2.add("song2-1");
        song2.add("song1-2");

        Album album1 = new Album("album1", song1);
        Album album2 = new Album("album2", song1);
        Album album3 = new Album("album3", song1);
        Album album4 = new Album("album4", song1);
        Album album5 = new Album("album5", song2);
        Album album6 = new Album("album6", song2);
        Album album7 = new Album("album7", song2);
        Album album8 = new Album("album8", song2);
        Album album9 = new Album("album9", song2);
        Album album10 = new Album("album10", song2);
        listAlbum.add(album1);
        listAlbum.add(album2);
        listAlbum.add(album3);
        listAlbum.add(album4);
        listAlbum.add(album5);
        listAlbum.add(album6);
        listAlbum.add(album7);
        listAlbum.add(album8);
        listAlbum.add(album9);
        listAlbum.add(album10);
    }

    public static List<Album> getListAlbum() {
        return listAlbum;
    }

    public static void setListAlbum(List<Album> listAlbum) {
        AlbumService.listAlbum = listAlbum;
    }

}
