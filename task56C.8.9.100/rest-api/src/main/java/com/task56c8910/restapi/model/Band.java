package com.task56c8910.restapi.model;

import java.util.List;

public class Band {
    private String bandName;
    private List<BandMember> member;
    private List<Album> albums;

    public Band(String bandName, List<BandMember> member, List<Album> albums) {
        this.bandName = bandName;
        this.member = member;
        this.albums = albums;
    }

    public Band() {
    }

    public String getBandName() {
        return bandName;
    }

    public List<BandMember> getMember() {
        return member;
    }

    public List<Album> getAlbums() {
        return albums;
    }

    public void addAlbum() {

    }

}
