package com.task56c8910.restapi.controller;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.task56c8910.restapi.model.Band;
import com.task56c8910.restapi.model.Composer;
import com.task56c8910.restapi.service.ComposerService;

@RestController
public class ComposerController {
    @CrossOrigin
    @GetMapping("/all-band")
    public List<Band> getBand() throws Exception {
        List<Band> allBand = ComposerService.getListBand();
        return allBand;
    }

}
