package com.task56c8910.restapi.model;

public class Composer extends Person {
    private String stagename;

    public Composer(String firstName, String lastName, String stagename) {
        super(firstName, lastName);
        this.stagename = stagename;
    }

    public String getStagename() {
        return stagename;
    }

}
