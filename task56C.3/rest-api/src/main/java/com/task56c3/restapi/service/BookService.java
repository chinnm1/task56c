package com.task56c3.restapi.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.task56c3.restapi.model.Book;

public class BookService {
    public static List<Book> bookList = new ArrayList<Book>();
    @Autowired
    static AuthorService authorService;

    public BookService() {
    }

    static {
        Book book1 = new Book("book1", authorService.getListAuthorBook1(), 10000, 10);
        Book book2 = new Book("book", authorService.getListAuthorBook2(), 10000, 5);
        bookList.add(book1);
        bookList.add(book2);
    }

    public static List<Book> getBookList() {
        return bookList;
    }

    public static AuthorService getAuthorService() {
        return authorService;
    }

    public static void setBookList(List<Book> bookList) {
        BookService.bookList = bookList;
    }

    public static void setAuthorService(AuthorService authorService) {
        BookService.authorService = authorService;
    }

}
