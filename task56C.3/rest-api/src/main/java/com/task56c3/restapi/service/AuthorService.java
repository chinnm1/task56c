package com.task56c3.restapi.service;

import java.util.ArrayList;
import java.util.List;

import com.task56c3.restapi.model.Author;

public class AuthorService {
    private static List<Author> listAuthorBook1 = new ArrayList<Author>();
    private static List<Author> listAuthorBook2 = new ArrayList<Author>();
    private static List<Author> allListAuthor = new ArrayList<Author>();

    static {
        Author authorbook1a = new Author("authorbook1a", "authorbook1a@gmail.com", 'f');
        Author authorbook1b = new Author("authorbook1b", "authorbook1b@gmail.com", 'f');
        Author authorbook2a = new Author("authorbook2a", "authorbook2a@gmail.com", 'f');
        Author authorbook2b = new Author("authorbook2b", "authorbook2b@gmail.com", 'f');

        listAuthorBook1.add(authorbook1a);
        listAuthorBook1.add(authorbook1b);
        listAuthorBook2.add(authorbook2a);
        listAuthorBook2.add(authorbook2b);
        allListAuthor.add(authorbook1a);
        allListAuthor.add(authorbook1b);
        allListAuthor.add(authorbook2a);
        allListAuthor.add(authorbook2a);
        allListAuthor.add(authorbook2b);

    }

    public static List<Author> getListAuthorBook1() {
        return listAuthorBook1;
    }

    public static List<Author> getListAuthorBook2() {
        return listAuthorBook2;
    }

    public static List<Author> getAllListAuthor() {
        return allListAuthor;
    }

    public static void setListAuthorBook1(List<Author> listAuthorBook1) {
        AuthorService.listAuthorBook1 = listAuthorBook1;
    }

    public static void setListAuthorBook2(List<Author> listAuthorBook2) {
        AuthorService.listAuthorBook2 = listAuthorBook2;
    }

    public static void setAllListAuthor(List<Author> allListAuthor) {
        AuthorService.allListAuthor = allListAuthor;
    }

}
