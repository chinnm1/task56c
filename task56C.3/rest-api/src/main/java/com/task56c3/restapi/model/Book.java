package com.task56c3.restapi.model;

import java.util.List;

public class Book {
    private String name;
    private List<Author> listAuthor;
    private double price;
    private int qty;

    public Book(String name, List<Author> listAuthor, double price, int qty) {
        this.name = name;
        this.listAuthor = listAuthor;
        this.price = price;
        this.qty = qty;
    }

    public Book() {
    }

    public String getName() {
        return name;
    }

    public List<Author> getListAuthor() {
        return listAuthor;
    }

    public double getPrice() {
        return price;
    }

    public int getQty() {
        return qty;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setListAuthor(List<Author> listAuthor) {
        this.listAuthor = listAuthor;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

}
