package com.devcamp.task56c.j56c;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class J56cApplication {

	public static void main(String[] args) {
		SpringApplication.run(J56cApplication.class, args);
	}

}
