package com.devcamp.task56c.j56c.service;

import java.util.ArrayList;

import com.devcamp.task56c.j56c.model.*;

public class CountryService {

    private Country country;
    private ArrayList<Region> regions;
    public CountryService() {
        Country country = new Country( "Vietnamese","VN");
        Region region01 = new Region("HN","Ha Noi");
        Region region02 = new Region("SG","Sai Gon");

        this.country = country;
        this.regions = new ArrayList<Region>();
        this.regions.add(region01);
        this.regions.add(region02);
        this.country.setRegions(this.regions);
        this.regions.add(new Region("DN","Da Nang"));
    }
    public CountryService(String countryCode, String countryName) {
        Country country = new Country(countryName,countryCode);
        Region region01 = new Region("HN","Ha Noi");
        Region region02 = new Region("SG","Sai Gon");

        this.country = country;
        this.regions = new ArrayList<Region>();
        this.regions.add(region01);
        this.regions.add(region02);
        this.country.setRegions(this.regions);
        this.regions.add(new Region("DN","Da Nang"));
    }
    public CountryService(String countryCode, String countryName, ArrayList<Region> regions) {
        Country country = new Country(countryName,countryCode);
        this.country = country;
        this.regions = regions;
    }
    public Country getCountry() {
        return this.country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public ArrayList<Region> getRegions() {
        return this.regions;
    }

    public void setRegions(ArrayList<Region> regions) {
        this.regions = regions;
    }
    
}
