package com.devcamp.task56c.j56c.controller;
import java.util.*;

import org.springframework.web.bind.annotation.*;

import com.devcamp.task56c.j56c.model.Country;
import com.devcamp.task56c.j56c.model.Region;
import com.devcamp.task56c.j56c.service.CountryService;


@RestController
public class CountryController {
    @CrossOrigin
    @GetMapping("/countries")
    public ArrayList<Country> getCountryList() {
        ArrayList<Country> countries = new ArrayList<Country>();
        countries.add((new CountryService()).getCountry());
        countries.add((new CountryService("US", "Americal")).getCountry());
        return countries;
    }
    @CrossOrigin
    @GetMapping("/country")
    public ArrayList<Country> getCountry(@RequestParam(value = "code", defaultValue = "VN") String name) {
        ArrayList<Country> countries = new ArrayList<Country>();
        countries.add((new CountryService()).getCountry());
        countries.add((new CountryService("US", "Americal")).getCountry());
        ArrayList<Country> countriesByCode = new ArrayList<Country>();
        for (Country country : countries) {
            if(country.getCountryCode().equalsIgnoreCase(name)){
                countriesByCode.add(country);
            }
        }
        return countriesByCode;
    }
    @CrossOrigin
    @GetMapping("/region")
    public ArrayList<Region> getRegions(@RequestParam(value = "code", defaultValue = "HN") String name) {
        ArrayList<Country> countries = new ArrayList<Country>();
        countries.add((new CountryService()).getCountry());
        countries.add((new CountryService("US", "Americal")).getCountry());
        ArrayList<Region> regionByCode = new ArrayList<Region>();
        for (Country country : countries) {
            ArrayList<Region> list = country.getRegions();
            for (Region region : list) {
                if (region.getRegionCode().equalsIgnoreCase(name)){
                    regionByCode.add(region);
                }
            }
        }
        return regionByCode;
    }
}
