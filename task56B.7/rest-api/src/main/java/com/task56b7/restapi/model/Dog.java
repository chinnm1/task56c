package com.task56b7.restapi.model;

public class Dog extends Mammal {

    public Dog(String name) {
        super(name);
    }

    public void greets() {
        System.out.println("Woof");
    }

    public void greets(Dog another) {
        System.out.println("Woooooof");
    }

    @Override
    public String toString() {
        return String.format("Dog [Mammal[Animal[Name = %s]]]", name);
    }

}
