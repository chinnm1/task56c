package com.task56c2.restapi.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.task56c2.restapi.model.Artist;
import com.task56c2.restapi.service.ArtistService;

@RestController
public class ArtistController {
    @CrossOrigin
    @GetMapping("/artists")
    public List<Artist> getartist() throws Exception {
        List<Artist> allArtist = ArtistService.getArtistsList();
        return allArtist;
    }

    @GetMapping("/artist-info")
    public Map<String, Object> getRegionOfCOuntry(@RequestParam(required = true, name = "artistId") int id)
            throws Exception {
        Map<String, Object> returnObj = new HashMap<String, Object>();
        Artist artist = null;
        int i = 0;
        boolean isFound = false;
        while (!isFound == true && i < ArtistService.getArtistsList().size()) {
            if (ArtistService.getArtistsList().get(i).getId() == id) {
                artist = ArtistService.getArtistsList().get(i);
                isFound = true;
                returnObj.put("artist", artist);
                returnObj.put("status", new String("ok"));

            } else {
                i++;
                returnObj.put("artist", null);
                returnObj.put("status", new String("not found"));

            }

        }
        return returnObj;

    }
}
